package com.cihangir.security;

import com.cihangir.model.Admin;
import com.cihangir.model.Customer;
import com.cihangir.service.CustomerService;
import com.cihangir.service.impl.CustomerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cihangir on 7/16/17.
 */

@Component
public class CustomerCustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private CustomerService customerService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        //O anki oturum bilgileri aliniyor.
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        Customer customer=customerService.getCustomerByUsername(username);

        if (customer == null) {
            throw new BadCredentialsException("Username not found.");
        }

        if (!password.equals(customer.getPassword())) {
            throw new BadCredentialsException("Wrong password.");
        }

        //Yeni bir yetki listesi olusturulur.
        List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();

        //Yetki listesine kullanicinin rolü eklenir.
        grantedAuths.add(new SimpleGrantedAuthority("ROLE_USER"));

//        final UserDetails principal = new User(username, password, grantedAuths);


        //Bu kullaniciya ait oturum icin yeni bir token olusturulur.
        //Icerisine kullanici bilgileri ve yetkileri atanir.
        Authentication auth = new UsernamePasswordAuthenticationToken(username, password, grantedAuths);

        return auth;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
}
