package com.cihangir.security;

import com.cihangir.model.Admin;
import com.cihangir.service.AdminService;
import com.cihangir.service.impl.AdminServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cihangir on 7/14/17.
 */

@Component
public class AdminCustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private AdminServiceImpl adminService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        //O anki oturum bilgileri aliniyor.
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        Admin admin = adminService.findAdminByName(username);

        if (admin == null) {
            throw new BadCredentialsException("Username not found.");
        }

        if (!password.equals(admin.getPassword())) {
            throw new BadCredentialsException("Wrong password.");
        }

        //Yeni bir yetki listesi olusturulur.
        List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();

        //Yetki listesine kullanicinin rolü eklenir.
        grantedAuths.add(new SimpleGrantedAuthority("ROLE_ADMIN"));

//        final UserDetails principal = new User(username, password, grantedAuths);


        //Bu kullaniciya ait oturum icin yeni bir token olusturulur.
        //Icerisine kullanici bilgileri ve yetkileri atanir.
        Authentication auth = new UsernamePasswordAuthenticationToken(username, password, grantedAuths);

        return auth;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
}
