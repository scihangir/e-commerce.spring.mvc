package com.cihangir.service;

import com.cihangir.model.Cart;

/**
 * Created by cihangir on 7/19/17.
 */
public interface CartService {
    Cart getCartById (Long cartId);
    void updateCartGroundTotal(Cart cart);
}
