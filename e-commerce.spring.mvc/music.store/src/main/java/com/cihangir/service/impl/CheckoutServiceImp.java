package com.cihangir.service.impl;

import com.cihangir.dao.CheckoutDAO;
import com.cihangir.model.Checkout;
import com.cihangir.service.CheckoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by cihangir on 7/23/17.
 */

@Service
@Transactional
public class CheckoutServiceImp implements CheckoutService {

    @Autowired
    private CheckoutDAO checkoutDAO;

    @Override
    public void addCheckout(Checkout checkout) {
        checkoutDAO.addCheckout(checkout);
    }
}
