package com.cihangir.service;

import com.cihangir.model.Admin;

/**
 * Created by cihangir on 7/14/17.
 */
public interface AdminService {
    public Admin findAdminByName(String username);
}
