package com.cihangir.service.impl;

import com.cihangir.dao.CartItemDAO;
import com.cihangir.model.Cart;
import com.cihangir.model.CartItem;
import com.cihangir.service.CartItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by cihangir on 7/18/17.
 */

@Service
@Transactional
public class CartItemServiceImpl implements CartItemService {

    @Autowired
    private CartItemDAO cartItemDAO;

    public void addCartItem(CartItem cartItem) {
        cartItemDAO.addCartItem(cartItem);
    }

    public void removeCartItem(CartItem cartItem) {
        cartItemDAO.removeCartItem(cartItem);
    }

    public void removeAllCartItems(Cart cart) {
        cartItemDAO.removeAllCartItems(cart);
    }

    public CartItem getCartItemByProductId(Long productId) {
        return cartItemDAO.getCartItemByProductId(productId);
    }



}
