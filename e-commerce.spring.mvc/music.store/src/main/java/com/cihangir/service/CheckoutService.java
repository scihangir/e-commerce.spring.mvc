package com.cihangir.service;

import com.cihangir.model.Checkout;

/**
 * Created by cihangir on 7/23/17.
 */
public interface CheckoutService {
    void addCheckout(Checkout checkout);
}
