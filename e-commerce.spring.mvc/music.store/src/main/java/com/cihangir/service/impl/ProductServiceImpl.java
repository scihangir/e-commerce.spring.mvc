package com.cihangir.service.impl;

import com.cihangir.dao.ProductDAO;
import com.cihangir.model.Product;
import com.cihangir.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by cihangir on 7/10/17.
 */
@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDAO productDAO;

    public List<Product> getAllProducts() {
        return productDAO.getAllProducts();
    }

    public Product getProductById(Long productID) {
        return productDAO.getProductById(productID);
    }

    public void addProduct(Product product) {
        productDAO.addProduct(product);
    }

    @Override
    public void deleteProduct(Product product) {
        productDAO.deleteProduct(product);
    }
}
