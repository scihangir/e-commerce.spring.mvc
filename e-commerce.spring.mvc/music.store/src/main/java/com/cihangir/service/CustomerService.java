package com.cihangir.service;

import com.cihangir.model.Customer;

import java.util.List;

/**
 * Created by cihangir on 7/15/17.
 */
public interface CustomerService {
    void addCustomer(Customer customer);
    Customer getCustomerById (Long customerId);

    Customer getCustomerByUsername (String username);

    List<Customer> getAllCustomers();
}
