package com.cihangir.service;

import com.cihangir.model.Product;

import java.util.List;

/**
 * Created by cihangir on 7/10/17.
 */
public interface ProductService {

    List<Product> getAllProducts();

    Product getProductById(Long productID);


    void addProduct(Product product);

    void deleteProduct(Product product);
}
