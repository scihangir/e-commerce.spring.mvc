package com.cihangir.service;

import com.cihangir.model.Cart;
import com.cihangir.model.CartItem;

/**
 * Created by cihangir on 7/18/17.
 */
public interface CartItemService {
    void addCartItem(CartItem cartItem);

    void removeCartItem (CartItem cartItem);

    void removeAllCartItems (Cart cart);

    CartItem getCartItemByProductId (Long productId);
}
