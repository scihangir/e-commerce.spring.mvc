package com.cihangir.service.impl;

import com.cihangir.dao.CartDAO;
import com.cihangir.model.Cart;
import com.cihangir.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by cihangir on 7/19/17.
 */

@Service
@Transactional
public class CartServiceImp implements CartService {

    @Autowired
    private CartDAO cartDAO;

    @Override
    public Cart getCartById(Long cartId) {
        return cartDAO.getCartById(cartId);
    }

    @Override
    public void updateCartGroundTotal(Cart cart) {
        cartDAO.updateCartGroundTotal(cart);
    }




}
