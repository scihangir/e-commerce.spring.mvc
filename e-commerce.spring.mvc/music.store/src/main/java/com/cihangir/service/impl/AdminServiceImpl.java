package com.cihangir.service.impl;

import com.cihangir.dao.AdminDAO;
import com.cihangir.model.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by cihangir on 7/14/17.
 */

@Service
@Transactional
public class AdminServiceImpl {

    @Autowired
    private AdminDAO adminDAO;

    public Admin findAdminByName(String username) {
        return adminDAO.findAdminByName(username);
    }


}
