package com.cihangir.controller.customer;

import com.cihangir.model.Address;
import com.cihangir.model.Cart;
import com.cihangir.model.Customer;
import com.cihangir.model.Product;
import com.cihangir.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by cihangir on 7/15/17.
 */

@Controller
@RequestMapping("/customer")
public class RegisterController {

    @Autowired
    private CustomerService customerService;


    @RequestMapping("/register")
    public String showCustomerRegisterForm(@ModelAttribute("newCustomer") Customer newProduct) {
        return "customer/register";
    }
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerCustomerPost(@ModelAttribute("newCustomer") Customer customer) {


        customer.setCart(new Cart());
        customerService.addCustomer(customer);

        return "customer/register-success";
    }



}
