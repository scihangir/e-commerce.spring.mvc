package com.cihangir.controller.admin;

import com.cihangir.model.Admin;
import com.cihangir.model.Product;
import com.cihangir.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by cihangir on 7/14/17.
 */

@Controller
@RequestMapping("/admin")
public class AdminHomeController {

    @Autowired
    private ProductService productService;



    @RequestMapping
    public String adminPage() {
        return "admin";
    }

    @RequestMapping("/productInventory")
    public String productInventory(Model model) {
        List<Product> products = productService.getAllProducts();
        model.addAttribute("products", products);

        return "admin/product-inventory";
    }

//    @RequestMapping("/customer")
//    public String customerManagement(Model model) {
//        List<Customer> customerList = customerService.getAllCustomers();
//        model.addAttribute(customerList);
//
//        return "customerManagement";
//    }
}
