package com.cihangir.controller.customer;

import com.cihangir.model.Cart;
import com.cihangir.model.CartItem;
import com.cihangir.model.Customer;
import com.cihangir.model.Product;
import com.cihangir.service.CartItemService;
import com.cihangir.service.CartService;
import com.cihangir.service.CustomerService;
import com.cihangir.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;


/**
 * Created by cihangir on 7/17/17.
 */
@Controller
@RequestMapping("/customer/cart")
public class CartController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ProductService productService;

    @Autowired
    private CartItemService cartItemService;

    @Autowired
    private CartService cartService;



    @RequestMapping("/add/{productId}")
    public String addItem(@PathVariable(value = "productId") Long productId) {

        //aktif olan kullanici getiriliyor ...
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String activeUser = authentication.getName();

        //sepete eklenecek urun
        Product product=productService.getProductById(productId);

        //musteri ve sepeti
        Customer customer=customerService.getCustomerByUsername(activeUser);
        Cart cart=customer.getCart();

        //musterin sepetindeki urunler
        List<CartItem> cartItems = cart.getCartItems();

        //eklenecek urun sepette varsa
        for (int i = 0; i < cartItems.size(); i++) {
            if (product.getId() == cartItems.get(i).getProduct().getId()) {
                CartItem cartItem = cartItems.get(i);
                cartItem.setQuantity(cartItem.getQuantity() + 1);
                cartItem.setTotalPrice(product.getUnitPrice() * cartItem.getQuantity());
                cartItemService.addCartItem(cartItem);

                return "redirect:/customer/cart/";

            }
        }

        //eklenecek urun sepette yoksa
        CartItem cartItem=new CartItem();
        cartItem.setProduct(product);
        cartItem.setQuantity(1);
        cartItem.setTotalPrice(product.getUnitPrice()*cartItem.getQuantity());
        cartItem.setCart(cart);

        cartItemService.addCartItem(cartItem);

        return "redirect:/customer/cart/";
    }



    @RequestMapping("/")
    public String getCart(Model theModel) {

        //aktif olan kullanici getiriliyor ...
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String activeUser = authentication.getName();

        //musteri ve sepeti
        Customer customer=customerService.getCustomerByUsername(activeUser);
        Cart cart=customer.getCart();

        // sepetin toplam fiyati guncelleniyor ...
        cartService.updateCartGroundTotal(cart);

        theModel.addAttribute("cart", cartService.getCartById( cart.getId()) );
        return "customer/cart";
    }


    @RequestMapping(value = "/remove/{productId}")
    public String removeItem(@PathVariable(value = "productId") Long productId) {

        //aktif olan kullanici getiriliyor ...
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String activeUser = authentication.getName();

        //musteri ve sepeti
        Customer customer=customerService.getCustomerByUsername(activeUser);
        Cart cart=customer.getCart();


        CartItem cartItem = cartItemService.getCartItemByProductId(productId);
        System.out.println("CartController.removeItem"+cartItem.getProduct());


        cartItemService.removeCartItem(cartItem);


        return "redirect:/customer/cart/";
    }



    @RequestMapping(value = "/clear/{cartId}")
    public String delete(@PathVariable(value = "cartId") Long cartId) {
        Cart cart = cartService.getCartById(cartId);
        cartItemService.removeAllCartItems(cart);

        return "redirect:/customer/cart/";
    }






}
