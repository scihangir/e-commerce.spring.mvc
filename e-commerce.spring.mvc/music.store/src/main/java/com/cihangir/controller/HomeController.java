package com.cihangir.controller;

import com.cihangir.model.Product;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.List;

/**
 * Created by cihangir on 7/5/17.
 */
@Controller
public class HomeController {


    @RequestMapping("/")
    public String helloWorld() {
        return "home";
    }
    @RequestMapping("/login")
    public String getLoginPage() {
        return "login";
    }



//    @RequestMapping("/productList")
//    public String  getProducts(Model model) {
//        List<Product> productList=productDAO.getProductList();
//
//        model.addAttribute("products", productList);
//
//
//        return "productList";
//    }
//
//    @RequestMapping("/productList/viewProduct/{productId}")
//    public String viewProduct(@PathVariable int productId, Model model) throws IOException {
//
//        Product theProduct=productDAO.getProductById(productId);
//        model.addAttribute("product", theProduct);
//
//        return "viewProduct";
//    }
//

}
