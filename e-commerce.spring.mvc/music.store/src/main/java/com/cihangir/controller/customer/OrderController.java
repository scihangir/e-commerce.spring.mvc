package com.cihangir.controller.customer;

import com.cihangir.model.Cart;
import com.cihangir.model.Checkout;
import com.cihangir.model.Customer;
import com.cihangir.model.Product;
import com.cihangir.service.CartService;
import com.cihangir.service.CustomerService;
import com.cihangir.service.CheckoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by cihangir on 7/23/17.
 */

@Controller
@RequestMapping("/customer/order")
public class OrderController {


    @Autowired
    private CustomerService customerService;

    @Autowired
    private CheckoutService checkoutService;

    @Autowired
    private CartService cartService;



    @RequestMapping("/{cartId}")
    public String createOrder (@PathVariable("cartId") Long cartId, Model theModel) {

        Checkout checkout=new Checkout();


        //aktif olan kullanici getiriliyor ...
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String activeUser = authentication.getName();

        //musteri
        Customer customer=customerService.getCustomerByUsername(activeUser);
        checkout.setCustomer(customer);


        checkout.setCart( customer.getCart() );
        checkout.setAddress( customer.getAddress() );



        // sepetin toplam fiyati guncelleniyor ...
        cartService.updateCartGroundTotal(customer.getCart());

        checkoutService.addCheckout(checkout);
        theModel.addAttribute("order", checkout);

        return "customer/order";
    }

}
