package com.cihangir.controller.customer;

import com.cihangir.model.Cart;
import com.cihangir.model.Customer;
import com.cihangir.model.Product;
import com.cihangir.service.CustomerService;
import com.cihangir.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by cihangir on 7/16/17.
 */

@Controller
@RequestMapping("/customer")
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private CustomerService customerService;


    @RequestMapping("/productInventory")
    public String productInventory(Model model) {
        List<Product> products = productService.getAllProducts();
        model.addAttribute("products", products);

        return "customer/product-inventory";
    }

    @RequestMapping("/product")
    public String getProductById(Model model, @RequestParam("id") Long productId) {
        Product product = productService.getProductById(productId);



        //aktif olan kullanici getiriliyor ...
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String activeUser = authentication.getName();

        //musteri ve sepeti
        Customer customer=customerService.getCustomerByUsername(activeUser);
        Cart cart=customer.getCart();

        // urun sayfasinda view product butonuna tiklanildiginda
        // kullanicinin sepetinin id'si gerekli
        model.addAttribute("cartId",cart.getId());


        model.addAttribute("product", product);

        return "customer/product";
    }


}

