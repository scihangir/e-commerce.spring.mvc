package com.cihangir.dao;

import com.cihangir.model.Cart;
import com.cihangir.model.CartItem;
import com.cihangir.model.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.List;

/**
 * Created by cihangir on 7/19/17.
 */
@Repository
public class CartDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Cart getCartById (Long cartId) {
        Session session = sessionFactory.getCurrentSession();
        Cart cart = (Cart) session.get(Cart.class, cartId);
        session.flush();

        return cart;
    }

    public void updateCartGroundTotal(Cart cart) {

        Session session = sessionFactory.getCurrentSession();

        //================================================
        double grandTotal=0;

        List<CartItem> cartItems=cart.getCartItems();

        for (CartItem item :cartItems) {
            grandTotal = grandTotal + item.getTotalPrice();
        }

        cart.setGrandTotal(grandTotal);
        //================================================

        session.saveOrUpdate(cart);
        session.flush();

    }



}
