package com.cihangir.dao;

import com.cihangir.model.Checkout;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by cihangir on 7/23/17.
 */

@Repository
public class CheckoutDAO {

    @Autowired
    private SessionFactory sessionFactory;


    public void addCheckout(Checkout checkout) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(checkout);
        session.flush();
    }
}
