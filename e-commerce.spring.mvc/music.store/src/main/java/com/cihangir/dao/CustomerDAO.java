package com.cihangir.dao;

import com.cihangir.model.Admin;
import com.cihangir.model.Customer;
import com.cihangir.model.Product;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by cihangir on 7/15/17.
 */

@Repository
public class CustomerDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public void addCustomer(Customer customer) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(customer);
        session.flush();
    }

    public Customer getCustomerById(Long customerId) {
        Session session = sessionFactory.getCurrentSession();
        Customer customer = (Customer) session.get(Customer.class, customerId);
        session.flush();

        return customer;
    }

    public Customer getCustomerByUsername(String username) {
        Criteria criteria=sessionFactory.getCurrentSession().createCriteria(Customer.class);
        criteria.add(Restrictions.eq("username", username));

        return (Customer) criteria.uniqueResult();
    }

    public List<Customer> getAllCustomers() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Customer");
        List<Customer> customerList =  query.list();
        session.flush();

        return customerList;
    }
}
