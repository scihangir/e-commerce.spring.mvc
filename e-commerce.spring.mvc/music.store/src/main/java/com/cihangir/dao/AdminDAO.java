package com.cihangir.dao;

import com.cihangir.model.Admin;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by cihangir on 7/14/17.
 */
@Repository
public class AdminDAO {

    @Autowired
    private SessionFactory sessionFactory;


    public Admin findAdminByName(String username) {
        Criteria criteria=sessionFactory.getCurrentSession().createCriteria(Admin.class);
        criteria.add(Restrictions.eq("username", username));

        return (Admin) criteria.uniqueResult();
    }

}
