package com.cihangir.dao;

import com.cihangir.model.Product;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by cihangir on 7/10/17.
 */

@Repository
public class ProductDAO {
    @Autowired
    private SessionFactory sessionFactory;

    public List<Product> getAllProducts() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Product");
        List<Product> productList =  query.list();
        session.flush();

        return productList;
    }

    public Product getProductById(Long productID) {
        Session session = sessionFactory.getCurrentSession();
        Product product = (Product) session.get(Product.class, productID);
        session.flush();

        return product;
    }

    public void addProduct(Product product) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(product);
        session.flush();
    }

    public void deleteProduct(Product product) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(product);
        session.flush();
    }

}
