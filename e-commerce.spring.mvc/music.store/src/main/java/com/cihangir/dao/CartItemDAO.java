package com.cihangir.dao;

import com.cihangir.model.Cart;
import com.cihangir.model.CartItem;
import com.cihangir.model.Customer;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by cihangir on 7/18/17.
 */

@Repository
public class CartItemDAO {
    @Autowired
    private SessionFactory sessionFactory;

    public void addCartItem (CartItem cartItem) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(cartItem);
        session.flush();
    }

    public void removeCartItem (CartItem cartItem) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(cartItem);
        session.flush();
    }

    public void removeAllCartItems (Cart cart) {
        List<CartItem> cartItems = cart.getCartItems();

        for (CartItem item : cartItems) {
            removeCartItem(item);
        }
    }

    public CartItem getCartItemByProductId (Long productId) {

        Criteria criteria=sessionFactory.getCurrentSession().createCriteria(CartItem.class);
        criteria.add(Restrictions.eq("product.id", productId));

        return (CartItem) criteria.uniqueResult();

    }
}
