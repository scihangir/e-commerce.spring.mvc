<%--
  Created by IntelliJ IDEA.
  User: cihangir
  Date: 7/14/17
  Time: 12:11 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <title>Login</title>
    <style>
        .loginPanel {
            width: 320px;
            margin-top: 100px;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
    <!-- Bootstrap core CSS -->
    <link href="${contextPath}/resources/css/bootstrap.min.css"  rel="stylesheet">

</head>
<body>
<div class="container">
    <div class="header">
        <ul class="nav nav-pills pull-right">
            <li>
                <a href="${contextPath}/">Home</a>
            </li>
            <li>
                <a href="${contextPath}/about">About</a>
            </li>
            <li>
                <a href="${contextPath}/login">Login</a>
            </li>
            <li>
                <a href="${contextPath}/register">Register</a>
            </li>
        </ul>
        <h3 class="text-muted">Web Store</h3>
    </div>

    <div class="jumbotron">
        <h1>
            Login
        </h1>
        <p>
            User Login
        </p>
    </div>
    <div class="row">
        <div class="loginPanel">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">Giriş Paneli</div>
                </div>
                <div class="panel-body">
                    <form method="post" action="/j_spring_security_check"/>
                    <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-user"></i>
                            </span>
                        <input id="j_username" type="text" class="form-control" name="j_username" placeholder="Kullanıcı Adı">
                    </div>
                    <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-lock"></i>
                            </span>
                        <input id="j_password" type="password" class="form-control" name="j_password" placeholder="Şifre">
                    </div>
                    <button type="submit" class="btn btn-success btn-block">Giriş</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
