<%--
  Created by IntelliJ IDEA.
  User: cihangir
  Date: 7/14/17
  Time: 6:28 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <title>Admin</title>
    <!-- Bootstrap core CSS -->
    <link href="${contextPath}/resources/css/bootstrap.min.css"  rel="stylesheet">
</head>
<body>
<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>Administrator page</h1>

            <p class="lead">This is the administrator page!</p>
        </div>

        <c:if test="${pageContext.request.userPrincipal.name != null}">
        <h2>
            Welcome: ${pageContext.request.userPrincipal.name} |
            <a href="${contextPath}/j_spring_security_logout">Logout</a>
        </h2>
        </c:if>

        <h3>
            <a href="${contextPath}/admin/productInventory">Product Inventory</a>
        </h3>

        <p>Here you can view, check and modify the product inventory!</p>

        <h3>
            <a href="${contextPath}/admin/customer">Customer Management</a>
        </h3>

        <p>Here you can manage customer information!</p>
    </div>
</div>
</body>
</html>
