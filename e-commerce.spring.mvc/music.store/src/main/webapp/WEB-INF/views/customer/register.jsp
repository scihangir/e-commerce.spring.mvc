<%--
  Created by IntelliJ IDEA.
  User: cihangir
  Date: 7/15/17
  Time: 9:49 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<html>
<head>
    <link href="${contextPath}/resources/css/bootstrap.min.css"  rel="stylesheet">
    <title>Customer Register</title>
</head>
<body>
<div class="container">
    <div class="header">
        <ul class="nav nav-pills pull-right">
            <li>
                <a href="${contextPath}/customer/productInventory">Home</a>
            </li>
            <li>
                <a href="${contextPath}/about">About</a>
            </li>
            <li>
                <a href="${contextPath}/login">Login</a>
            </li>
            <li>
                <a href="${contextPath}/customer/register">Register</a>
            </li>
        </ul>
        <h3 class="text-muted">Web Store</h3>
    </div>

    <div class="jumbotron">
        <h1>
            Register Customer
        </h1>
    </div>
    <div class="row">
        <form:form class="form-horizontal" action="${contextPath}/customer/register" method="post" modelAttribute="newCustomer">
            <fieldset>

                <!-- Form Name -->
                <legend> Personal Information </legend>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label">Name</label>
                    <div class="col-md-6  inputGroupContainer">
                        <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <form:input  path="customerName" placeholder="Name" class="form-control"  type="text"/>
                        </div>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label">E-Mail</label>
                    <div class="col-md-6  inputGroupContainer">
                        <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                            <form:input path="customerEmail" placeholder="E-Mail Address" class="form-control"  type="text"/>
                        </div>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label">Phone</label>
                    <div class="col-md-6  inputGroupContainer">
                        <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                            <form:input path="customerPhone" placeholder="(555)-555-5555" class="form-control" type="text"/>
                        </div>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label">Username</label>
                    <div class="col-md-6  inputGroupContainer">
                        <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <form:input  path="username" placeholder="Username" class="form-control" type="text"/>
                        </div>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group has-feedback">
                    <label for="password"  class="col-md-4 control-label">Password</label>
                    <div class="col-md-6  inputGroupContainer">
                        <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                            <form:input path="password" class="form-control" type="password" placeholder="password" />
                        </div>
                    </div>
                </div>
            </fieldset>
            <legend> Address information </legend>
            <fieldset>
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label">Street</label>
                    <div class="col-md-6  inputGroupContainer">
                        <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                            <form:input path="address.street" placeholder="Street" class="form-control" type="text"/>
                        </div>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label">State</label>
                    <div class="col-md-6  inputGroupContainer">
                        <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                            <form:input path="address.state" placeholder="State" class="form-control" type="text"/>
                        </div>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label">Apartment Number</label>
                    <div class="col-md-6  inputGroupContainer">
                        <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                            <input name="apartmentNumber" placeholder="Apartment Number" class="form-control" type="text">
                        </div>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label">Town</label>
                    <div class="col-md-6  inputGroupContainer">
                        <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                            <form:input path="address.city" placeholder="City" class="form-control" type="text"/>
                        </div>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label">City</label>
                    <div class="col-md-6  inputGroupContainer">
                        <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                            <form:input path="address.province" placeholder="Province" class="form-control" type="text"/>
                        </div>
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-4">
                        <button type="submit" class="btn btn-warning">Save
                            <span class="glyphicon glyphicon-send"></span>
                        </button>
                    </div>
                </div>
            </fieldset>
        </form:form>
    </div>
</div>
</body>
</html>
