<%--
  Created by IntelliJ IDEA.
  User: cihangir
  Date: 7/12/17
  Time: 10:33 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>


<html>
<head>
    <!-- Bootstrap core CSS -->
    <link href="${contextPath}/resources/css/bootstrap.min.css"  rel="stylesheet">
    <title>Product</title>
</head>
<div class="container">
    <div class="header">
        <h3 class="text-muted">Web Store</h3>
        <ul class="nav nav-pills pull-right">
            <li>
                <a href="${contextPath}/customer/productInventory">
                    <span class="glyphicon glyphicon-info-sign"/></span>
                    Products
                </a>
            <li>
                <a href="${contextPath}/customer/cart/">
                    <span class="glyphicon glyphicon-shopping-cart"/></span>
                    Cart
                </a>
            </li>
            </li>
        </ul>
    </div>
    <div class="jumbotron">
        <h1>
            Products
        </h1>
        <p>
            Product
        </p>
        <p class="pull-right">
            <c:if test="${pageContext.request.userPrincipal.name != null}">
                <button type="button" class="btn btn-success">
                    <span class="glyphicon glyphicon-user"/></span>
                    Welcome ${pageContext.request.userPrincipal.name}</button>
                <a href="${contextPath}/j_spring_security_logout" class="btn btn-danger">
                    <span class="glyphicon glyphicon-log-out"/></span>
                    Logout
                </a>
            </c:if>
        </p>
    </div>

    <div class="row">
        <div class="col-md-5">
            <img src="${contextPath}/resources/images/${product.productId}.png" alt="image"  style = "width:100%"/>
        </div>

        <div class="col-md-5">
            <h3>${product.productName}</h3>
            <p>${product.productDescription}</p>
            <p>
                <strong>Item Code : </strong><span class="label label-warning">${product.productId}</span>
            </p>
            <p>
                <strong>manufacturer</strong> : ${product.productManufacturer}
            </p>
            <p>
                <strong>category</strong> : ${product.productCategory}
            </p>
            <p>
                <strong>Condition</strong> : ${product.productCondition}
            </p>
            <p>
                <strong>Availble units in stock </strong> : ${product.unitsInStock}
            </p>
            <h4>${product.unitPrice} USD</h4>
            <p>
                <a href="${contextPath}/customer/cart/add/${product.id}" class="btn btn-warning btn-large">
                    <span class="glyphicon-shopping-cart glyphicon"></span> Order Now </a>
                <a href="${contextPath}/customer/cart/" class="btn btn-default">
                    <span class="glyphicon-hand-right glyphicon"></span> View Cart
                </a>
                <a href="${contextPath}/customer/productInventory" class="btn btn-default">
                    <span class="glyphicon-hand-left glyphicon"></span> back
                </a>

            </p>

        </div>
    </div>
</div>

</html>
