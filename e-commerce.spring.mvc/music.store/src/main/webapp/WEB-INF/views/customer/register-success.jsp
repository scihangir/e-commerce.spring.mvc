<%--
  Created by IntelliJ IDEA.
  User: cihangir
  Date: 7/16/17
  Time: 12:14 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>Register Success</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css"  rel="stylesheet">
</head>
<body>

<div class="container">
    <div class="header">
        <ul class="nav nav-pills pull-right">
            <li>
                <a href="${contextPath}/customer/productInventory">Home</a>
            </li>
            <li>
                <a href="${contextPath}/about">About</a>
            </li>
            <li>
                <a href="${contextPath}/login">Login</a>
            </li>
            <li>
                <a href="${contextPath}/customer/register">Register</a>
            </li>
        </ul>
        <h3 class="text-muted">Web Store</h3>
    </div>

    <section>
        <div class="jumbotron">
            <div class="container">
                <h1 class="alert alert-danger"> Customer registered successfully!</h1>
            </div>
        </div>
    </section>
</div>
</body>
</html>
