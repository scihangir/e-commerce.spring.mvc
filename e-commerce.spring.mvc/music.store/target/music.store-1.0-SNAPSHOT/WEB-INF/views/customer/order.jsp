<%--
  Created by IntelliJ IDEA.
  User: cihangir
  Date: 7/23/17
  Time: 11:35 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<jsp:useBean id="now" class="java.util.Date"/>

<html>
<head>
    <title>Order Confirmation</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css"  rel="stylesheet">
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-wrapper">
    <div class="container">
        <section>
            <div class="jumbotron">
                <div class="container">
                    <h1>Order</h1>
                    <p>Order Confirmation</p>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <%--<form:form commandName="order" class="form-horizontal">--%>
                    <div class="well col-xs-10 col-sm-10 col-md-6 col-xs-offset-1 col-sm-offset-1 col-md-offset-3">
                        <div class="text-center">
                            <h1>Receipt</h1>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <address>
                                    <strong>Shipping Address</strong>
                                    <br>
                                        ${order.address.street}
                                    <br>
                                        ${order.address.apartmentNumber}
                                    <br>
                                        ${order.address.city},${order.address.state}
                                    <br>
                                        ${order.address.province}
                                    <br>
                                </address>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                                <p>
                                    <em>Shipping Date: <fmt:formatDate type="date" value="${now}"/></em>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>#</th>
                                    <th class="text-center">Price</th>
                                    <th class="text-center">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="cartItem" items="${order.cart.cartItems}">
                                    <tr>
                                        <td class="col-md-9">
                                            <em>${cartItem.product.productName}</em>
                                        </td>
                                        <td class="col-md-1" style="text-align: center">
                                            ${cartItem.quantity}
                                        </td>
                                        <td class="col-md-1 text-center">$${cartItem.product.unitPrice}</td>
                                        <td class="col-md-1 text-center">$${cartItem.totalPrice}</td>
                                    </tr>
                                </c:forEach>

                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right"><h4>
                                        <strong>Grand Total: </strong>
                                    </h4></td>
                                    <td class="text-center text-danger"><h4>
                                        <strong>$${order.cart.grandTotal}</strong>
                                    </h4></td>
                                </tr>
                                </tbody>
                            </table>
                            <button id="back" class="btn btn-default">back
                            </button>

                            <button type="submit" class="btn btn-success">
                                Submit Order <span class="glyphicon glyphicon-chevron-right"></span>
                            </button>

                            <button id="btnCancel" class="btn btn-default">Cancel</button>
                        </div>
                    </div>
                <%--</form:form>--%>
            </div>
        </div>
    </div>
</div>




</body>
</html>
