<%--
  Created by IntelliJ IDEA.
  User: cihangir
  Date: 7/19/17
  Time: 12:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<html>
<head>
    <title>Cart</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css"  rel="stylesheet">
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="header">
        <h3 class="text-muted">Web Store</h3>
        <ul class="nav nav-pills pull-right">
            <li>
                <a href="${contextPath}/customer/productInventory">
                    <span class="glyphicon glyphicon-info-sign"/></span>
                    Products
                </a>
            </li>
            <li>
                <a href="${contextPath}/customer/cart/">
                    <span class="glyphicon glyphicon-shopping-cart"/></span>
                    Cart
                </a>
            </li>
            <li>
        </ul>
    </div>
    <div class="jumbotron">
        <div class="container">
            <h1>Cart</h1>

            <p>All the selected products in your shopping cart</p>
        </div>
        <p class="pull-right">
            <c:if test="${pageContext.request.userPrincipal.name != null}">
                <button type="button" class="btn btn-success">
                    <span class="glyphicon glyphicon-user"/></span>
                    Welcome ${pageContext.request.userPrincipal.name}</button>
                <a href="${contextPath}/j_spring_security_logout" class="btn btn-danger">
                    <span class="glyphicon glyphicon-off"/></span>
                    Logout
                </a>
            </c:if>
        </p>
    </div>
    <div class="row">
        <div>
            <a class="btn btn-warning pull-left" href="${contextPath}/customer/cart/clear/${cart.id }">
                <span class="glyphicon glyphicon-refresh"></span>Clear Cart
            </a>

            <a href="${contextPath}/customer/order/${cart.id}" class="btn btn-primary pull-right">
                <span class="glyphicon-shopping-cart glyphicon"></span> Check out
            </a>
        </div>
        <br><br><br>
        <table class="table table-hover">
            <tr>
                <th>Product</th>
                <th>Unit Price</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
            <c:forEach items="${cart.cartItems}" var="item">
                    <tr>
                        <td>${item.product.productName}</td>
                        <td>${item.product.unitPrice}</td>
                        <td>${item.quantity}</td>
                        <td>${item.totalPrice}</td>
                        <td>
                            <a class="label label-danger" href="${contextPath}/customer/cart/remove/${item.product.id}" >
                                <span class="glyphicon glyphicon-remove"></span>remove
                            </a>
                        </td>
                    </tr>
                    <tr>

            </c:forEach>
                <th></th>
                <th></th>
                <th>Grand Total</th>
                <th></th>
                <th>${cart.grandTotal}</th>
            </tr>
        </table>

        <a href="${contextPath}/customer/productInventory" class="btn btn-default">
            <span class="glyphicon glyphicon-share-alt"></span>
            Continue Shopping
        </a>

    </div>
</div>


</body>
</html>
