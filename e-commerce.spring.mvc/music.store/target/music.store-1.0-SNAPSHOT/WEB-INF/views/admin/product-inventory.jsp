<%--
  Created by IntelliJ IDEA.
  User: cihangir
  Date: 7/14/17
  Time: 7:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<html>
<head>
    <title>Title</title>
    <link href="${contextPath}/resources/css/bootstrap.min.css"  rel="stylesheet">
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <div class="header">
        <h3 class="text-muted">Web Store</h3>
        <ul class="nav nav-pills pull-right">
            <li>
                <a href="${contextPath}/admin">
                    <span class="glyphicon glyphicon-home"/></span>
                    Home
                </a>
            </li>
            <li>
                <a href="${contextPath}/admin/productInventory">
                    <span class="glyphicon glyphicon-info-sign"/></span>
                    Products
                </a>
            </li>
            <li>
            <li>
                <a href="${contextPath}/admin/add/">
                    <span class="glyphicon glyphicon-plus"/></span>
                    Add Product
                </a>
            </li>
        </ul>
    </div>
    <div class="jumbotron">
        <h1>
            Products

            <sec:authorize access="hasRole('ROLE_USER')">

                This content will only be visible to users who have
                the "supervisor" authority in their list of <tt>GrantedAuthority</tt>s.

            </sec:authorize>
        </h1>
        <p>
            Available Products
        </p>
        <p class="pull-right">
            <c:if test="${pageContext.request.userPrincipal.name != null}">
                <button type="button" class="btn btn-success">
                    <span class="glyphicon glyphicon-user"/></span>
                    Welcome ${pageContext.request.userPrincipal.name}</button>
                <a href="${contextPath}/j_spring_security_logout" class="btn btn-danger">
                    <span class="glyphicon glyphicon-log-out"/></span>
                    Logout
                </a>
            </c:if>
        </p>
    </div>
    <div class="row">
        <c:forEach items="${products}" var="product">
            <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
                <div class="thumbnail">
                    <img src="${contextPath}/resources/images/${product.productId}.png" alt="image"  style = "width:100%"/>
                    <div class="caption">
                        <h3>${product.productName}</h3>
                        <p>${product.productDescription}</p>
                        <p>${product.unitPrice}USD</p>
                        <p>Available ${product.unitsInStock} units in stock</p>
                        <p>
                            <a href="${contextPath}/admin/product?id=${product.id}" class="btn btn-primary">
                                <span class="glyphicon-info-sign glyphicon"/></span>
                            </a>
                            <a href="${contextPath}/admin/editProduct/${product.id}" class="btn btn-warning">
                                <span class="glyphicon-edit glyphicon"/></span>
                            </a>
                            <a href="${contextPath}/admin/deleteProduct/${product.id}" class="btn btn-danger" onclick="return confirm('Bu ürünü silmek istediğinize emin misiz?')">
                                <span class="glyphicon-remove glyphicon"/></span>
                            </a>
                        </p>

                    </div>
                </div>
            </div>
        </c:forEach>
    </div>

</div>
</body>
</html>
