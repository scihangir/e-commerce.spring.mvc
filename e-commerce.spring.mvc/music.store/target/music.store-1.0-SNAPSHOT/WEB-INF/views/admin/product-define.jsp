<%--
  Created by IntelliJ IDEA.
  User: cihangir
  Date: 7/10/17
  Time: 1:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <!-- Bootstrap core CSS -->
    <link href="${contextPath}/resources/css/bootstrap.min.css"  rel="stylesheet">
    <title>Products</title>
</head>
<body>
<div class="container">
    <div class="header">
        <h3 class="text-muted">Web Store</h3>
        <ul class="nav nav-pills pull-right">
            <li>
                <a href="${contextPath}/admin">
                    <span class="glyphicon glyphicon-home"/></span>
                    Home
                </a>
            </li>
            <li>
                <a href="${contextPath}/admin/productInventory">
                    <span class="glyphicon glyphicon-info-sign"/></span>
                    Products
                </a>
            </li>
            <li>
            <li>
                <a href="${contextPath}/admin/add/">
                    <span class="glyphicon glyphicon-plus"/></span>
                    Add Product
                </a>
            </li>
        </ul>
    </div>

    <div class="jumbotron">
        <div class="container">
            <h1>Products</h1>
            <p>Add products</p>
            <p class="pull-right">
                <c:if test="${pageContext.request.userPrincipal.name != null}">
                    <button type="button" class="btn btn-success">
                        <span class="glyphicon glyphicon-user"/></span>
                        Welcome ${pageContext.request.userPrincipal.name}</button>
                    <a href="${contextPath}/j_spring_security_logout" class="btn btn-danger">
                        <span class="glyphicon glyphicon-log-out"/></span>
                        Logout
                    </a>
                </c:if>
            </p>
        </div>
    </div>
    <div class="form-horizontal">
        <form:form  modelAttribute="newProduct" class="form-horizontal" enctype="multipart/form-data" action="${contextPath}/admin/add">
            <form:hidden path="id"/>
            <fieldset>
                <legend>Add new product</legend>

                <div class="form-group">
                    <label class="control-label col-lg-2 col-lg-2 control-label" for="productId">Product Id</label>
                    <div class="col-lg-1">
                        <form:input id="productId" path="productId" type="text" class="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2 control-label" for="name">Name</label>
                    <div class="col-lg-3">
                        <form:input id="name" path="productName" type="text" class="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2 control-label" for="unitPrice">Unit Price</label>
                    <div class="col-lg-1">
                        <form:input id="unitPrice" path="unitPrice" type="text" class="form-control"/>
                   </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2 control-label" for="description">Description</label>
                    <div class="col-lg-3">
                        <form:textarea id="description" path="productDescription" rows = "2" class="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2 control-label " for="manufacturer">Manufacturer</label>
                    <div class="col-lg-3">
                        <form:input id="manufacturer" path="productManufacturer" type="text" class="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2 control-label" for="category">Category</label>
                    <div class="col-lg-3">
                        <form:input id="category" path="productCategory" type="text" class="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2 control-label" for="unitsInStock">Unit In Stock</label>
                    <div class="col-lg-1">
                        <form:input id="unitsInStock" path="unitsInStock" type="text" class="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2 control-label" for="condition">Condition</label>
                    <div class="col-lg-10">
                        <form:radiobutton path="productCondition" value="New" />New
                        <form:radiobutton path="productCondition" value="Old" />Old
                        <form:radiobutton path="productCondition" value="Refurbished" />Refurbished
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2 control-label" for="productImage">Image</label>
                    <div class="col-lg-3">
                        <form:input id="productImage" path="productImage" type="file" class="form-control"/>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <input type="submit" id="btnAdd" class="btn btn-primary" value ="Add"/>
                    </div>
                </div>

            </fieldset>
        </form:form>
    </div>
</div>

</body>
</html>
