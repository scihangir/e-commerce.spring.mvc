<%--
  Created by IntelliJ IDEA.
  User: cihangir
  Date: 7/10/17
  Time: 12:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <title>Products</title>

    <!-- Bootstrap core CSS -->
    <link href="${contextPath}/resources/css/bootstrap.min.css"  rel="stylesheet">

</head>
<body>
<div class="container">
    <div class="header">
        <ul class="nav nav-pills pull-right">
            <li>
                <a href="${contextPath}/">Home</a>
            </li>
            <li>
                <a href="${contextPath}/about">About</a>
            </li>
            <li>
                <a href="${contextPath}/login">Login</a>
            </li>
            <li>
                <a href="${contextPath}/customer/register">Register</a>
            </li>
        </ul>
        <h3 class="text-muted">Web Store</h3>
    </div>

    <div class="jumbotron">
        <h1>
            Products
        </h1>
        <p>
            Available Products
        </p>
    </div>
    <div class="row">

        <%--<c:forEach items="${products}" var="product">--%>
            <%--<div class="col-sm-6 col-md-3" style="padding-bottom: 15px">--%>
                <%--<div class="thumbnail">--%>
                    <%--<img src="${contextPath}/resources/images/${product.productId}.png" alt="image"  style = "width:100%"/>--%>
                    <%--<div class="caption">--%>
                        <%--<h3>${product.productName}</h3>--%>
                        <%--<p>${product.productDescription}</p>--%>
                        <%--<p>${product.unitPrice}USD</p>--%>
                        <%--<p>Available ${product.unitsInStock} units in stock</p>--%>
                        <%--<p>--%>
                            <%--<a href="${contextPath}/product?id=${product.id}" class="btn btn-primary">--%>
                                <%--<span class="glyphicon-info-sign glyphicon"/></span> Details--%>
                            <%--</a>--%>
                        <%--</p>--%>

                    <%--</div>--%>
                <%--</div>--%>
            <%--</div>--%>
        <%--</c:forEach>--%>
    </div>
</div>
</body>
</html>
